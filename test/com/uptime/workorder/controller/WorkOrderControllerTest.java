/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WorkOrderControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//workorders.cql", true, true, "worldview_dev1"));

    static WorkOrderController instance;
    private static String content = "";
    static String rowId;
    
    @BeforeClass
    public static void setUpClass() {
        instance = new WorkOrderController();
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "\"priority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-21T01:20:00Z\",\n"
                + "\"action\": \"Test Action\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 1\",\n"
                + "\"description\": \"Test description 1\",\n"
                + "\"issue\": \"Test issue 1\",\n"
                + "\"notes\": \"Test notes 1\",\n"
                + "\"user\": \"Test user 1\",\n"
                + "\"trendDetails\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\"}\n"
                + "]\n"
                + "}";
    }
    
    /**
     * Test of createOpenWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test10_CreateOpenWorkOrder() {
        System.out.println("createOpenWorkOrder");
        String expResult = "{\"outcome\":\"New WorkOrder created successfully.\"}";
        String result = instance.createOpenWorkOrder(content);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createOpenWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test11_CreateOpenWorkOrder() {
        System.out.println("createOpenWorkOrder");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                //+ "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\n"
                + "\"priority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-22T05:20:00Z\",\n"
                + "\"action\": \"Test Action 2\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 2\",\n"
                + "\"description\": \"Test description 2\",\n"
                + "\"issue\": \"Test issue 2\",\n"
                + "\"notes\": \"Test notes 2\",\n"
                + "\"user\": \"Test user 2\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dad\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b6\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9727\",\n"
                + "    \"paramName\" : \"TruePeak\"}\n"
                + "]\n"
                + "}";
        String result = instance.createOpenWorkOrder(content);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createOpenWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test111_CreateOpenWorkOrder() {
        System.out.println("createOpenWorkOrder");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createOpenWorkOrder("");
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createOpenWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test12_CreateOpenWorkOrder() {
        System.out.println("createOpenWorkOrder");
        String expResult = "{\"outcome\":\"New WorkOrder created successfully.\"}";
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\n"
                + "\"priority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-22T05:20:00Z\",\n"
                + "\"action\": \"Test Action 2\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 2\",\n"
                + "\"description\": \"Test description 2\",\n"
                + "\"issue\": \"Test issue 2\",\n"
                + "\"notes\": \"Test notes 2\",\n"
                + "\"user\": \"Test user 2\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dad\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b6\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9727\",\n"
                + "    \"paramName\" : \"TruePeak\"}\n"
                + "]\n"
                + "}";
        String result = instance.createOpenWorkOrder(content);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createClosedWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test20_CreateClosedWorkOrder() {
        System.out.println("createClosedWorkOrder");
        String expResult = "{\"outcome\":\"WorkOrder closed successfully.\"}";
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\n"
                + "\"priority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-22T05:20:00Z\",\n"
                + "\"closeDate\": \"2022-10-26T07:20:00Z\",\n"
                + "\"action\": \"Test Action 2\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 2\",\n"
                + "\"description\": \"Test description 2\",\n"
                + "\"issue\": \"Test issue 2\",\n"
                + "\"notes\": \"Test notes 2\",\n"
                + "\"user\": \"Test user 2\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dad\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b6\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9727\",\n"
                + "    \"paramName\" : \"TruePeak\"}\n"
                + "]\n"
                + "}";
        String result = instance.createClosedWorkOrder(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createClosedWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test21_CreateClosedWorkOrder() {
        System.out.println("createClosedWorkOrder");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                //+ "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\n"
                + "\"priority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-22T05:20:00Z\",\n"
                + "\"closeDate\": \"2022-10-26T07:20:00Z\",\n"
                + "\"action\": \"Test Action 2\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 2\",\n"
                + "\"description\": \"Test description 2\",\n"
                + "\"issue\": \"Test issue 2\",\n"
                + "\"notes\": \"Test notes 2\",\n"
                + "\"user\": \"Test user 2\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dad\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b6\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9727\",\n"
                + "    \"paramName\" : \"TruePeak\"}\n"
                + "]\n"
                + "}";
        String result = instance.createClosedWorkOrder(content);
        System.out.println("test21_CreateClosedWorkOrder result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createClosedWorkOrder method, of class WorkOrderController.
     */
    @Test
    public void test22_CreateClosedWorkOrder() {
        System.out.println("createClosedWorkOrder");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createClosedWorkOrder("");
        System.out.println("test21_CreateClosedWorkOrder result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSite method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_GetOpenWorkOrdersByCustomerSite() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSite");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 1\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSite(customerAccount, siteId);
        System.out.println("result 30 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSite method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_GetOpenWorkOrdersByCustomerSite() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSite");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSite(customerAccount, siteId);
        System.out.println("result 31 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSite method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetOpenWorkOrdersByCustomerSite() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSite");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-";
        instance.getOpenWorkOrdersByCustomerSite(customerAccount, siteId);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test40_GetOpenWorkOrdersByCustomerSiteArea() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteArea");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 1\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSiteArea(customerAccount, siteId, areaId);
        System.out.println("result 40 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test41_GetOpenWorkOrdersByCustomerSiteArea() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteArea");
        String customerAccount = "FEDEX11";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSiteArea(customerAccount, siteId, areaId);
        System.out.println("result 41 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test42_GetOpenWorkOrdersByCustomerSiteArea() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteArea");
        String customerAccount = "FEDEX11";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b583";
        instance.getOpenWorkOrdersByCustomerSiteArea(customerAccount, siteId, areaId);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteAreaAsset method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test50_GetOpenWorkOrdersByCustomerSiteAreaAsset() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteAreaAsset");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 1\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
        System.out.println("result 50 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteAreaAsset method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test51_GetOpenWorkOrdersByCustomerSiteAreaAsset() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteAreaAsset");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
        System.out.println("result 51 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteAreaAsset method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test52_GetOpenWorkOrdersByCustomerSiteAreaAsset() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteAreaAsset");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        instance.getOpenWorkOrdersByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteAreaAssetPriority method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test60_GetOpenWorkOrdersByCustomerSiteAreaAssetPriority() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteAreaAssetPriority");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 1\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSiteAreaAssetPriority(customerAccount, siteId, areaId, assetId, priority);
        System.out.println("result 60 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteAreaAssetPriority method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test61_GetOpenWorkOrdersByCustomerSiteAreaAssetPriority() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteAreaAssetPriority");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getOpenWorkOrdersByCustomerSiteAreaAssetPriority(customerAccount, siteId, areaId, assetId, priority);
        System.out.println("result 61 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByCustomerSiteAreaAssetPriority method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test62_GetOpenWorkOrdersByCustomerSiteAreaAssetPriority() throws Exception {
        System.out.println("getOpenWorkOrdersByCustomerSiteAreaAssetPriority");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        instance.getOpenWorkOrdersByCustomerSiteAreaAssetPriority(customerAccount, siteId, areaId, assetId, priority);
    }

    /**
     * Test of getOpenWorkOrdersByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test70_GetOpenWorkOrdersByPK() throws Exception {
        System.out.println("getOpenWorkOrdersByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 1\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
        System.out.println("result 70 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test71_GetOpenWorkOrdersByPK() throws Exception {
        System.out.println("getOpenWorkOrdersByPK");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
        System.out.println("result 71 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getOpenWorkOrdersByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test72_GetOpenWorkOrdersByPK() throws Exception {
        System.out.println("getOpenWorkOrdersByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
    }

    /**
     * Test of getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test80_getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String result = instance.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate);
        rowId = result.substring(result.indexOf("\"rowId\":")+9, result.indexOf("\"rowId\":")+45);
        System.out.println("result 80 - " + result);
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"workOrderTrends\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"rowId\":\"" + rowId + "\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"paramName\":\"Peak2Peak\",\"channelType\":\"AC\"}],\"outcome\":\"GET worked successfully.\"}";
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test81_getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String result = instance.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate);
        System.out.println("result 81 - " + result);
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test82_getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        instance.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate);
    }

    /**
     * Test of getItemsByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test90_getItemsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getItemsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        boolean returnOpenWorkOrders = false;
        boolean returnWorkOrderTrends = false;
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getItemsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate, returnOpenWorkOrders, returnWorkOrderTrends);
        System.out.println("result 90 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getItemsByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test91_getItemsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getItemsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        boolean returnOpenWorkOrders = true;
        boolean returnWorkOrderTrends = false;
        instance.getItemsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate, returnOpenWorkOrders, returnWorkOrderTrends);
    }

    /**
     * Test of getByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test910_getItemsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getItemsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        boolean returnOpenWorkOrders = false;
        boolean returnWorkOrderTrends = true;
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"workOrderTrends\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"rowId\":\"" + rowId + "\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"paramName\":\"Peak2Peak\",\"channelType\":\"AC\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getItemsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate, returnOpenWorkOrders, returnWorkOrderTrends);
        System.out.println("result 910 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test911_getItemsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getItemsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX1111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-11-21T01:20:00Z";
        boolean returnOpenWorkOrders = false;
        boolean returnWorkOrderTrends = true;
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getItemsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate, returnOpenWorkOrders, returnWorkOrderTrends);
        System.out.println("result 911 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteAreaAssetPriorityDate method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test912_getItemsByCustomerSiteAreaAssetPriorityDate() throws Exception {
        System.out.println("getItemsByCustomerSiteAreaAssetPriorityDate");
        String customerAccount = "FEDEX1111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-";
        String priority = "4";
        String createdDate = "2022-11-21T01:20:00Z";
        boolean returnOpenWorkOrders = false;
        boolean returnWorkOrderTrends = true;
        instance.getItemsByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate, returnOpenWorkOrders, returnWorkOrderTrends);
    }

    /**
     * Test of getWorkOrderTrendsByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test920_GetWorkOrderTrendsByPK() throws Exception {
        System.out.println("getWorkOrderTrendsByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"rowId\":\"" + rowId + "\",\"workOrderTrends\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"rowId\":\"" + rowId + "\",\"pointLocationId\":\"665611d8-382f-4c95-b5af-a23432e59dac\",\"pointId\":\"60685b3e-758a-4295-a243-3d600d8d75b5\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"paramName\":\"Peak2Peak\",\"channelType\":\"AC\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getWorkOrderTrendsByPK(customerAccount, siteId, areaId, assetId, priority, createdDate, rowId);
        System.out.println("result 920 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderTrendsByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test921_GetWorkOrderTrendsByPK() throws Exception {
        System.out.println("getWorkOrderTrendsByPK");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getWorkOrderTrendsByPK(customerAccount, siteId, areaId, assetId, priority, createdDate, rowId);
        System.out.println("result 921 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderTrendsByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test922_GetWorkOrderTrendsByPK() throws Exception {
        System.out.println("getWorkOrderTrendsByPK");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        instance.getWorkOrderTrendsByPK(customerAccount, siteId, areaId, assetId, priority, createdDate, rowId);
    }

    /**
     * Test of getWorkOrderCountsByCustomerSite method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test930_GetWorkOrderCountsByCustomerSite() throws Exception {
        System.out.println("getWorkOrderCountsByCustomerSite");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"workOrderCounts\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority1\":0,\"priority2\":0,\"priority3\":0,\"priority4\":1,\"totalCount\":1},{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"priority1\":0,\"priority2\":0,\"priority3\":0,\"priority4\":0,\"totalCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getWorkOrderCountsByCustomerSite(customerAccount, siteId);
        System.out.println("result 930 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderCountsByCustomerSite method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test9301_GetWorkOrderCountsByCustomerSite() throws Exception {
        System.out.println("getWorkOrderCountsByCustomerSite");
        String customerAccount = "FEDEX11";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getWorkOrderCountsByCustomerSite(customerAccount, siteId);
        System.out.println("result 9301 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderCountsByCustomerSite method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test9302_GetWorkOrderCountsByCustomerSite() throws Exception {
        System.out.println("getWorkOrderCountsByCustomerSite");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-";
        instance.getWorkOrderCountsByCustomerSite(customerAccount, siteId);
    }

    /**
     * Test of getWorkOrderCountsByCustomerSiteArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test931_GetWorkOrderCountsByCustomerSiteArea() throws Exception {
        System.out.println("getWorkOrderCountsByCustomerSiteArea");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"workOrderCounts\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority1\":0,\"priority2\":0,\"priority3\":0,\"priority4\":1,\"totalCount\":1},{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"priority1\":0,\"priority2\":0,\"priority3\":0,\"priority4\":0,\"totalCount\":0}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getWorkOrderCountsByCustomerSiteArea(customerAccount, siteId, areaId);
        System.out.println("result 931 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getWorkOrderCountsByCustomerSiteArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test9311_GetWorkOrderCountsByCustomerSiteArea() throws Exception {
        System.out.println("getWorkOrderCountsByCustomerSiteArea");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getWorkOrderCountsByCustomerSiteArea(customerAccount, siteId, areaId);
        System.out.println("result 9311 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getWorkOrderCountsByCustomerSiteArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test9312_GetWorkOrderCountsByCustomerSiteArea() throws Exception {
        System.out.println("getWorkOrderCountsByCustomerSiteArea");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        instance.getWorkOrderCountsByCustomerSiteArea(customerAccount, siteId, areaId);
    }

    /**
     * Test of getWorkOrderCountsByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test932_GetWorkOrderCountsByPK() throws Exception {
        System.out.println("getWorkOrderCountsByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"workOrderCounts\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority1\":0,\"priority2\":0,\"priority3\":0,\"priority4\":1,\"totalCount\":1}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getWorkOrderCountsByPK(customerAccount, siteId, areaId, assetId);
        System.out.println("result 932 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderCountsByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test9321_GetWorkOrderCountsByPK() throws Exception {
        System.out.println("getWorkOrderCountsByPK");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getWorkOrderCountsByPK(customerAccount, siteId, areaId, assetId);
        System.out.println("result 932 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderCountsByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test9322_GetWorkOrderCountsByPK() throws Exception {
        System.out.println("getWorkOrderCountsByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"workOrderCounts\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority1\":0,\"priority2\":0,\"priority3\":0,\"priority4\":1,\"totalCount\":1}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getWorkOrderCountsByPK(customerAccount, siteId, areaId, assetId);
        System.out.println("result 932 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getClosedWorkOrdersByCustomerSiteYear method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test940_GetClosedWorkOrdersByCustomerSiteYear() throws Exception {
        System.out.println("getClosedWorkOrdersByCustomerSiteYear");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getClosedWorkOrdersByCustomerSiteYear(customerAccount, siteId, year);
        System.out.println("result 940 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getClosedWorkOrdersByCustomerSiteYear method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test941_GetClosedWorkOrdersByCustomerSiteYear() throws Exception {
        System.out.println("getClosedWorkOrdersByCustomerSiteYear");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getClosedWorkOrdersByCustomerSiteYear(customerAccount, siteId, year);
        System.out.println("result 940 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getClosedWorkOrdersByCustomerSiteYear method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test942_GetClosedWorkOrdersByCustomerSiteYear() throws Exception {
        System.out.println("getClosedWorkOrdersByCustomerSiteYear");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-94a83aa67";
        String year = "2022";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getClosedWorkOrdersByCustomerSiteYear(customerAccount, siteId, year);
        System.out.println("result 940 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteYearArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test950_GetByCustomerSiteYearArea() throws Exception {
        System.out.println("getByCustomerSiteYearArea");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteYearArea(customerAccount, siteId, year, areaId);
        System.out.println("result 950 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteYearArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test951_GetByCustomerSiteYearArea() throws Exception {
        System.out.println("getByCustomerSiteYearArea");
        String customerAccount = "FEDEX1111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getByCustomerSiteYearArea(customerAccount, siteId, year, areaId);
        System.out.println("result 950 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteYearArea method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test952_GetByCustomerSiteYearArea() throws Exception {
        System.out.println("getByCustomerSiteYearArea");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-8e8e843";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getByCustomerSiteYearArea(customerAccount, siteId, year, areaId);
        System.out.println("result 950 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteYearAreaAsset method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test960_GetClosedWorkOrdersByCustomerSiteYearAreaAsset() throws Exception {
        System.out.println("getClosedWorkOrdersByCustomerSiteYearAreaAsset");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "9e3c16da-1a28-4a31-81ff-8ef46aa23cfa";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getClosedWorkOrdersByCustomerSiteYearAreaAsset(customerAccount, siteId, year, areaId, assetId);
        System.out.println("result 960 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteYearAreaAsset method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test961_GetClosedWorkOrdersByCustomerSiteYearAreaAsset() throws Exception {
        System.out.println("getClosedWorkOrdersByCustomerSiteYearAreaAsset");
        String customerAccount = "FEDEX111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "9e3c16da-1a28-4a31-81ff-8ef46aa23cfa";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getClosedWorkOrdersByCustomerSiteYearAreaAsset(customerAccount, siteId, year, areaId, assetId);
        System.out.println("result 960 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getByCustomerSiteYearAreaAsset method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test962_GetClosedWorkOrdersByCustomerSiteYearAreaAsset() throws Exception {
        System.out.println("getClosedWorkOrdersByCustomerSiteYearAreaAsset");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "9e3c16da-1a28-4a31-81ff-8ef46aa23cfa";
        instance.getClosedWorkOrdersByCustomerSiteYearAreaAsset(customerAccount, siteId, year, areaId, assetId);
    }

    /**
     * Test of getClosedWorkOrdersByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test970_GetClosedWorkOrdersByPK() throws Exception {
        System.out.println("getClosedWorkOrdersByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "9e3c16da-1a28-4a31-81ff-8ef46aa23cfa";
        String createdDate = "2022-10-22T05:20:00Z";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00Z\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getClosedWorkOrdersByPK(customerAccount, siteId, year, areaId, assetId, createdDate);
        System.out.println("result 970 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getClosedWorkOrdersByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test971_GetClosedWorkOrdersByPK() throws Exception {
        System.out.println("getClosedWorkOrdersByPK");
        String customerAccount = "FEDEX1111";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "9e3c16da-1a28-4a31-81ff-8ef46aa23cfa";
        String createdDate = "2022-10-22T05:20:00Z";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getClosedWorkOrdersByPK(customerAccount, siteId, year, areaId, assetId, createdDate);
        System.out.println("result 970 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getClosedWorkOrdersByPK method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test972_GetClosedWorkOrdersByPK() throws Exception {
        System.out.println("getClosedWorkOrdersByPK");
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String year = "2022";
        String areaId = "247b9503-42bc-44d6-8e843";
        String assetId = "9e3c16da-1a28-4a31-81ff-8ef46aa23cfa";
        String createdDate = "2022-10-22T05:20:00Z";
        String expResult = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"year\":\"2022\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00Z\",\"closedWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"openedYear\":2022,\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",\"createdDate\":\"2022-10-22T05:20:00+0000\",\"workOrderDetails\":{\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 2\",\"assetName\":\"Test Asset 1\",\"priority\":\"4\",\"closeDate\":\"2022-10-26T07:20:00+0000\",\"action\":\"Test Action 2\",\"description\":\"Test description 2\",\"issue\":\"Test issue 2\",\"notes\":\"Test notes 2\",\"user\":\"Test user 2\"}}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getClosedWorkOrdersByPK(customerAccount, siteId, year, areaId, assetId, createdDate);
        System.out.println("result 970 - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateOpenWorkOrder method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test980_UpdateOpenWorkOrder() throws Exception {
        System.out.println("updateOpenWorkOrder");
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "\"priority\": \"4\",\n"
                + "\"newPriority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-21T01:20:00Z\",\n"
                + "\"action\": \"Test Action\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 10\",\n"
                + "\"description\": \"Test description 1\",\n"
                + "\"issue\": \"Test issue 1\",\n"
                + "\"notes\": \"Test notes 1, Test Append note\",\n"
                + "\"user\": \"Test user 1\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\"}\n"
                + "]\n"
                + "}";
        String expResult = "{\"outcome\":\"Updated Open WorkOrder items successfully.\"}";
        String result = instance.updateOpenWorkOrder(content);
        System.out.println("result 980 - " + result);
        assertEquals(expResult, result);
        
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult1 = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 10\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1, Test Append note\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result1 = instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
        System.out.println("result 981 - " + result1);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of updateOpenWorkOrder method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test981_UpdateOpenWorkOrder() throws Exception {
        System.out.println("updateOpenWorkOrder");
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
//                + "\"priority\": \"4\",\n"
                + "\"newPriority\": \"4\",\n"
                + "\"createdDate\": \"2022-10-21T01:20:00Z\",\n"
                + "\"action\": \"Test Action\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 10\",\n"
                + "\"description\": \"Test description 1\",\n"
                + "\"issue\": \"Test issue 1\",\n"
                + "\"notes\": \"Test notes 1, Test Append note\",\n"
                + "\"user\": \"Test user 1\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\"}\n"
                + "]\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateOpenWorkOrder(content);
        System.out.println("result 980 - " + result);
        assertEquals(expResult, result);
        
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult1 = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"4\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":4,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 10\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1, Test Append note\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        String result1 = instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
        System.out.println("result 981 - " + result1);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of updateOpenWorkOrder method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test982_UpdateOpenWorkOrder() throws Exception {
        System.out.println("updateOpenWorkOrder");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateOpenWorkOrder("");
        System.out.println("result 980 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateOpenWorkOrder method, of class WorkOrderController.
     * @throws java.lang.Exception
     */
    @Test
    public void test990_UpdateOpenWorkOrder() throws Exception {
        System.out.println("updateOpenWorkOrder");
        content = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "    \"siteId\" : \"7ac748c0-afca-400c-a315-53c94a83aa67\",\n"
                + "    \"areaId\" : \"247b9503-42bc-44d6-b58f-a573d8e8e843\",\n"
                + "    \"assetId\" : \"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\n"
                + "\"priority\": \"4\",\n"
                + "\"newPriority\": \"3\",\n"
                + "\"createdDate\": \"2022-10-21T01:20:00Z\",\n"
                + "\"action\": \"Test Action\",\n"
                + "\"areaName\": \"Test Area 1\",\n"
                + "\"assetName\": \"Test Asset 1\",\n"
                + "\"assetComponent\": \"Test assetComponent 10\",\n"
                + "\"description\": \"Test description 1\",\n"
                + "\"issue\": \"Test issue 1\",\n"
                + "\"notes\": \"Test notes 1, Test Append note\",\n"
                + "\"user\": \"Test user 1\",\n"
                + "\"trends\": [\n"
                + "{\"channelType\": \"AC\",\n"
                + "    \"pointLocationId\" : \"665611d8-382f-4c95-b5af-a23432e59dac\",\n"
                + "    \"pointId\" : \"60685b3e-758a-4295-a243-3d600d8d75b5\",\n"
                + "    \"apSetId\" : \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "    \"paramName\" : \"Peak2Peak\"}\n"
                + "]\n"
                + "}";
        String expResult = "{\"outcome\":\"Updated Open WorkOrder items successfully.\"}";
        String result = instance.updateOpenWorkOrder(content);
        System.out.println("result 990 - " + result);
        assertEquals(expResult, result);
        
        String customerAccount = "FEDEX";
        String siteId = "7ac748c0-afca-400c-a315-53c94a83aa67";
        String areaId = "247b9503-42bc-44d6-b58f-a573d8e8e843";
        String assetId = "3384f9d0-d1ff-4690-bcc9-57cba146f992";
        String priority = "4";
        String createdDate = "2022-10-21T01:20:00Z";
        String expResult1 = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result1 = instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
        System.out.println("result1 990 - " + result1);
        assertEquals(expResult1, result1);
        
        String result2 = instance.getOpenWorkOrdersByPK(customerAccount, siteId, areaId, assetId, "3", createdDate);
        System.out.println("result2 990  - " + result2);
        String expResult2 = "{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":\"3\",\"createdDate\":\"2022-10-21T01:20:00Z\",\"openWorkOrders\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"7ac748c0-afca-400c-a315-53c94a83aa67\",\"areaId\":\"247b9503-42bc-44d6-b58f-a573d8e8e843\",\"assetId\":\"3384f9d0-d1ff-4690-bcc9-57cba146f992\",\"priority\":3,\"createdDate\":\"2022-10-21T01:20:00+0000\",\"action\":\"Test Action\",\"areaName\":\"Test Area 1\",\"assetComponent\":\"Test assetComponent 10\",\"assetName\":\"Test Asset 1\",\"description\":\"Test description 1\",\"issue\":\"Test issue 1\",\"notes\":\"Test notes 1, Test Append note\",\"user\":\"Test user 1\"}],\"outcome\":\"GET worked successfully.\"}";
        assertEquals(expResult2, result2);
    }

}

