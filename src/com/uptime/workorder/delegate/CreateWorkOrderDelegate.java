/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.delegate;

import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.ClosedWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrderTrendsDAO;
import com.uptime.cassandra.workorder.dao.WorkOrderCountsDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.ClosedWorkOrders;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.workorder.WorkOrderService.sendEvent;
import com.uptime.workorder.utils.DelegateUtil;
import com.uptime.workorder.vo.WorkOrderVO;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class CreateWorkOrderDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateWorkOrderDelegate.class.getName());
    private final OpenWorkOrdersDAO openWorkOrdersDAO;
    private final ClosedWorkOrdersDAO closedWorkOrdersDAO;
    private final WorkOrderTrendsDAO workOrderTrendsDAO;
    private final WorkOrderCountsDAO workOrderCountsDAO;
    
    
    /**
     * Constructor
     */
    public CreateWorkOrderDelegate() {
        openWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
        closedWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().closedWorkOrdersDAO();
        workOrderTrendsDAO = WorkOrdersMapperImpl.getInstance().workOrderTrendsDAO();
        workOrderCountsDAO = WorkOrdersMapperImpl.getInstance().workOrderCountsDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     * @param workOrderVO, WorkOrderVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws java.lang.Exception
     */
    public String createOpenWorkOrder(WorkOrderVO workOrderVO) throws IllegalArgumentException, Exception {
        List<WorkOrderTrends> workOrderTrends = null;
        List<WorkOrderCounts> workOrderCountsList;
        WorkOrderCounts workOrderCounts = null;
        OpenWorkOrders openWorkOrders;
        WorkOrderCounts originalWorkOrderCounts = null;
        String errorMsg;
        
        if (workOrderVO != null) {
            
            // Getting workOrderCounts from DB
            try {
                if ((workOrderCountsList =  workOrderCountsDAO.findByPK(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId())) != null && !workOrderCountsList.isEmpty()) {
                    originalWorkOrderCounts = workOrderCountsList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to get original WorkOrder count.\"}";
            }

            // Set CreatedDate Field if null
            if (workOrderVO.getCreatedDate() == null) {
                workOrderVO.setCreatedDate(Instant.now().atZone(ZoneOffset.UTC).toInstant().truncatedTo(ChronoUnit.SECONDS));
            }

            // Set the workOrderTrends field
            if (workOrderVO.getTrendDetails() != null && !workOrderVO.getTrendDetails().isEmpty()) {
                workOrderTrends = DelegateUtil.getWorkOrderTrends(workOrderVO);
            }

            // Set the openWorkOrders field
            openWorkOrders = DelegateUtil.getOpenWorkOrders(workOrderVO);


            // Update the original workOrderCounts entity
            if (originalWorkOrderCounts != null) {
                switch (openWorkOrders.getPriority()) {
                    case 1:
                        originalWorkOrderCounts.setPriority1(originalWorkOrderCounts.getPriority1() + 1);
                        break;
                    case 2:
                        originalWorkOrderCounts.setPriority2(originalWorkOrderCounts.getPriority2() + 1);
                        break;
                    case 3:
                        originalWorkOrderCounts.setPriority3(originalWorkOrderCounts.getPriority3() + 1);
                        break;
                    case 4:
                        originalWorkOrderCounts.setPriority4(originalWorkOrderCounts.getPriority4() + 1);
                        break;
                    default:
                        break;
                }
                originalWorkOrderCounts.setTotalCount(originalWorkOrderCounts.getPriority1() + originalWorkOrderCounts.getPriority2() + originalWorkOrderCounts.getPriority3() + originalWorkOrderCounts.getPriority4());
            } 
            
            // If no original workOrderCounts entity found,
            // then create a new workOrderCounts entity
            else {
                workOrderCounts = DelegateUtil.getWorkOrderCounts(workOrderVO);
                switch (openWorkOrders.getPriority()) {
                    case 1:
                        workOrderCounts.setPriority1(1);
                        break;
                    case 2:
                        workOrderCounts.setPriority2(1);
                        break;
                    case 3:
                        workOrderCounts.setPriority3(1);
                        break;
                    case 4:
                        workOrderCounts.setPriority4(1);
                        break;
                    default:
                        break;
                }
                workOrderCounts.setTotalCount(1);
            }

            // Insert the entities into Cassandra
            try { 
                if (((errorMsg = ServiceUtil.validateObjectData(openWorkOrders)) == null) &&
                        ((errorMsg = originalWorkOrderCounts != null ? ServiceUtil.validateObjectData(originalWorkOrderCounts) : ServiceUtil.validateObjectData(workOrderCounts)) == null) &&
                        ((errorMsg = ServiceUtil.validateObjects(workOrderTrends, true)) == null)) {
                    
                    openWorkOrdersDAO.create(openWorkOrders);
                    if (workOrderTrends != null && !workOrderTrends.isEmpty()) {
                        workOrderTrends.forEach(wt -> workOrderTrendsDAO.create(wt));
                    }
                    if (originalWorkOrderCounts != null) {
                        workOrderCountsDAO.update(originalWorkOrderCounts);
                    } else if (workOrderCounts != null) {
                        workOrderCountsDAO.create(workOrderCounts);
                    }

                    return "{\"outcome\":\"New WorkOrder created successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to create new WorkOrder.\"}";
        }
        return "{\"outcome\":\"WorkOrderVO is null.\"}";
    }
   
    /**
     * Insert new Rows into Cassandra based on the given object
     * @param workOrderVO, WorkOrderVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws java.lang.Exception
     */
    public String createClosedWorkOrder(WorkOrderVO workOrderVO) throws IllegalArgumentException, Exception {
        List<WorkOrderTrends> workOrderTrendsList = null;
        List<OpenWorkOrders> openWorkOrdersList;
        List<WorkOrderCounts> workOrderCountsList;
        ClosedWorkOrders closedWorkOrders; 
        OpenWorkOrders openWorkOrders = null; 
        WorkOrderCounts workOrderCounts = null;
        String errorMsg;
        
        if (workOrderVO != null) {

            // Getting openWorkOrders from DB
            try {
                if((openWorkOrdersList = openWorkOrdersDAO.findByPK(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate())) != null && !openWorkOrdersList.isEmpty()) {
                    openWorkOrders = openWorkOrdersList.get(0);
                } else {
                    return "{\"outcome\":\"Open WorkOrder not found for the given Customer Account, Site Id, Area Id, Asset Id, Priority and Created Date.\"}";
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to validate Open WorkOrder.\"}";
            }
            
            // Getting openWorkOrders from DB
            try {
                workOrderTrendsList = workOrderTrendsDAO.findByCustomerSiteAreaAssetPriorityDate(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate());
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to validate WorkOrder Trends.\"}";
            }

            // Getting workOrderCounts from DB
            try {
                if ((workOrderCountsList =  workOrderCountsDAO.findByPK(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId())) != null && !workOrderCountsList.isEmpty()) {
                    workOrderCounts = workOrderCountsList.get(0);
                } else {
                    return "{\"outcome\":\"WorkOrder Count not found for the given Customer Account, Site Id, Area Id, and Asset Id.\"}";
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to get original WorkOrder count.\"}";
            }

            // Set the new closedWorkOrders field
            closedWorkOrders = DelegateUtil.getClosedWorkOrders(workOrderVO);

            // Update the workOrderCounts field
            switch (closedWorkOrders.getWorkOrderDetails().getPriority()) {
                case "1" :
                    workOrderCounts.setPriority1(Math.max(0, workOrderCounts.getPriority1() - 1));
                    break;
                case "2" :
                    workOrderCounts.setPriority2(Math.max(0, workOrderCounts.getPriority2() - 1));
                    break;
                case "3" :
                    workOrderCounts.setPriority3(Math.max(0, workOrderCounts.getPriority3() - 1));
                    break;
                case "4":
                    workOrderCounts.setPriority4(Math.max(0, workOrderCounts.getPriority4() - 1));
                    break;
                default:
                    break;
            }
            workOrderCounts.setTotalCount(workOrderCounts.getPriority1() + workOrderCounts.getPriority2() + workOrderCounts.getPriority3() + workOrderCounts.getPriority4());

            // Insert the entities into Cassandra
            try { 
                if (((errorMsg = ServiceUtil.validateObjectData(closedWorkOrders)) == null) &&
                        ((errorMsg = ServiceUtil.validateObjectData(workOrderCounts)) == null)) {
                    closedWorkOrdersDAO.create(closedWorkOrders);
                    workOrderCountsDAO.update(workOrderCounts);
                    openWorkOrdersDAO.delete(openWorkOrders);
                    if (workOrderTrendsList != null && !workOrderTrendsList.isEmpty()) {
                        workOrderTrendsDAO.deleteByCustomerSiteAreaAssetPriorityDate(workOrderTrendsList.get(0).getCustomerAccount(), workOrderTrendsList.get(0).getSiteId(), workOrderTrendsList.get(0).getAreaId(), workOrderTrendsList.get(0).getAssetId(), workOrderTrendsList.get(0).getPriority(), workOrderTrendsList.get(0).getCreatedDate());
                    }
                    return "{\"outcome\":\"WorkOrder closed successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to close WorkOrder.\"}";
        }
        return "{\"outcome\":\"WorkOrderVO is null.\"}";
    }
    
}
