/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.delegate;

import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrderCountsDAO;
import com.uptime.cassandra.workorder.dao.WorkOrderTrendsDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.workorder.WorkOrderService.sendEvent;
import com.uptime.workorder.utils.DelegateUtil;
import com.uptime.workorder.vo.WorkOrderVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateWorkOrderDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateWorkOrderDelegate.class.getName());
    private final OpenWorkOrdersDAO openWorkOrdersDAO;
    private final WorkOrderTrendsDAO workOrderTrendsDAO;
    private final WorkOrderCountsDAO workOrderCountsDAO;

    /**
     * Constructor
     */
    public UpdateWorkOrderDelegate() {
        openWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
        workOrderTrendsDAO = WorkOrdersMapperImpl.getInstance().workOrderTrendsDAO();
        workOrderCountsDAO = WorkOrdersMapperImpl.getInstance().workOrderCountsDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param workOrderVO, WorkOrderVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateOpenWorkOrder(WorkOrderVO workOrderVO) throws IllegalArgumentException {
        OpenWorkOrders newOpenWorkOrders;
        String errorMsg;

        if (workOrderVO != null) {
            
            // Set the newOpenWorkOrders field
            newOpenWorkOrders = DelegateUtil.getOpenWorkOrders(workOrderVO);
            
            // Insert updated entities into Cassandra if original entities are found
            try { 
                if (((errorMsg = ServiceUtil.validateObjectData(newOpenWorkOrders)) == null) &&
                        (errorMsg = updateTrendDetails(workOrderVO)) == null){
                    openWorkOrdersDAO.update(newOpenWorkOrders);
                    return "{\"outcome\":\"Updated Open WorkOrder items successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to update Open WorkOrder items.\"}";
        }
        return "{\"outcome\":\"WorkOrderVO is null.\"}";
    }

    /**
     * Helper method for the method updateOpenWorkOrder
     * 
     * @param workOrderVO, WorkOrderVO object
     * @throws IllegalArgumentException
     * @throws Exception 
     */
    private String updateTrendDetails(WorkOrderVO workOrderVO) throws IllegalArgumentException, Exception {
        List<WorkOrderTrends> workOrderTrendsFromDB;
        List<WorkOrderTrends> workOrderTrends = null;
        List<WorkOrderTrends> trendsNeedToAdd = null;
        List<WorkOrderTrends> trendsNeedToDel = null;
        String errorMsg;

        // Check if Trend Details are included, 
        // if so set the fields workOrderTrends and paramList
        if (workOrderVO.getTrendDetails() != null && !workOrderVO.getTrendDetails().isEmpty()) {
            workOrderTrends = DelegateUtil.getWorkOrderTrends(workOrderVO);
        }
        
        // Getting trends from DB
        workOrderTrendsFromDB = workOrderTrendsDAO.findByCustomerSiteAreaAssetPriorityDate(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate());

        // Check if any trends need to be added to DB
        if (workOrderTrends != null && !workOrderTrends.isEmpty()) {
            if (workOrderTrendsFromDB != null && !workOrderTrendsFromDB.isEmpty()) {
                for (WorkOrderTrends trend : workOrderTrends) {
                    if (!workOrderTrendsFromDB.stream().anyMatch(dbTrend -> trend.getPointLocationId().equals(dbTrend.getPointLocationId()) && trend.getPointId().equals(dbTrend.getPointId()) && trend.getParamName().equals(dbTrend.getParamName()))) {
                        if (trendsNeedToAdd == null) {
                            trendsNeedToAdd = new ArrayList();
                        }
                        trendsNeedToAdd.add(trend);
                    }
                }
            } else {
                trendsNeedToAdd = new ArrayList(workOrderTrends);
            }
        }

        // Check if any trends need to be removed from DB
        if (workOrderTrendsFromDB != null && !workOrderTrendsFromDB.isEmpty()) {
            if (workOrderTrends != null && !workOrderTrends.isEmpty()) {
                for (WorkOrderTrends dbTrend : workOrderTrendsFromDB) {
                    if (!workOrderTrends.stream().anyMatch(trend -> trend.getPointLocationId().equals(dbTrend.getPointLocationId()) && trend.getPointId().equals(dbTrend.getPointId()) && trend.getParamName().equals(dbTrend.getParamName()))) {
                        if (trendsNeedToDel == null) {
                            trendsNeedToDel = new ArrayList();
                        }
                        trendsNeedToDel.add(dbTrend);
                    }
                }
            } else {
                trendsNeedToDel = new ArrayList(workOrderTrendsFromDB);
            }
        }

        // Insert Trends into the DB
        if (trendsNeedToAdd != null) {
            for (WorkOrderTrends trend : trendsNeedToAdd) {
                if ((errorMsg = ServiceUtil.validateObjectData(trend)) == null) {
                    workOrderTrendsDAO.create(trend);
                } else {
                    return errorMsg;
                }
            }
        }
        
        // Delete Trends from the DB
        if (trendsNeedToDel != null) {
            for (WorkOrderTrends trend : trendsNeedToDel) {
                workOrderTrendsDAO.delete(trend);
            }
        }
        
        return null;
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param workOrderVO, WorkOrderVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteOldAndCreateNewWorkOrder(WorkOrderVO workOrderVO) throws IllegalArgumentException {
        List<WorkOrderTrends> originalWorkOrderTrendList = null;
        List<WorkOrderTrends> workOrderTrendList = null;
        List<OpenWorkOrders> openWorkOrdersList;
        List<WorkOrderCounts> workOrderCountsList;
        OpenWorkOrders originalOpenWorkOrders = null;
        OpenWorkOrders openWorkOrders;
        WorkOrderCounts originalWorkOrderCounts = null;
        WorkOrderCounts workOrderCounts = null;
        String errorMsg;

        if (workOrderVO != null) {
            
            // Getting workOrderCounts from DB
            try {
                if ((workOrderCountsList =  workOrderCountsDAO.findByPK(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId())) != null && !workOrderCountsList.isEmpty()) {
                    originalWorkOrderCounts = workOrderCountsList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to get original WorkOrder count.\"}";
            }
            
            // Getting openWorkOrders from DB
            try {
                if ((openWorkOrdersList = openWorkOrdersDAO.findByPK(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate())) != null && !openWorkOrdersList.isEmpty()) {
                    originalOpenWorkOrders = openWorkOrdersList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find Open WorkOrder for the given Customer Account, Site Id, Area Id, Asset Id, Priority and Created Date.\"}";
            }
            
            // Getting workOrderTrends from DB
            try {
                originalWorkOrderTrendList = workOrderTrendsDAO.findByCustomerSiteAreaAssetPriorityDate(workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate());
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to validate WorkOrder Trends.\"}";
            }
            
            // Update the original workOrderCounts entity
            if (originalWorkOrderCounts != null) {
                
                // Decrement Old Priority
                switch (workOrderVO.getPriority()) {
                    case 1:
                        originalWorkOrderCounts.setPriority1(Math.max(0, originalWorkOrderCounts.getPriority1() - 1));
                        break;
                    case 2:
                        originalWorkOrderCounts.setPriority2(Math.max(0, originalWorkOrderCounts.getPriority2() - 1));
                        break;
                    case 3:
                        originalWorkOrderCounts.setPriority3(Math.max(0, originalWorkOrderCounts.getPriority3() - 1));
                        break;
                    case 4:
                        originalWorkOrderCounts.setPriority4(Math.max(0, originalWorkOrderCounts.getPriority4() - 1));
                        break;
                    default:
                        break;
                }
                
                // Increment New Priority
                switch (workOrderVO.getNewPriority()) {
                    case 1:
                        originalWorkOrderCounts.setPriority1(originalWorkOrderCounts.getPriority1() + 1);
                        break;
                    case 2:
                        originalWorkOrderCounts.setPriority2(originalWorkOrderCounts.getPriority2() + 1);
                        break;
                    case 3:
                        originalWorkOrderCounts.setPriority3(originalWorkOrderCounts.getPriority3() + 1);
                        break;
                    case 4:
                        originalWorkOrderCounts.setPriority4(originalWorkOrderCounts.getPriority4() + 1);
                        break;
                    default:
                        break;
                }
                
                // Reset TotalCount Field
                // Note: TotalCount shouldn't need to be reset, this is just to ensure the correct values
                originalWorkOrderCounts.setTotalCount(originalWorkOrderCounts.getPriority1() + originalWorkOrderCounts.getPriority2() + originalWorkOrderCounts.getPriority3() + originalWorkOrderCounts.getPriority4());
            
            // If no original workOrderCounts entity found,
            // then create a new workOrderCounts entity
            } else {
                workOrderCounts = DelegateUtil.getWorkOrderCounts(workOrderVO);
                switch (workOrderVO.getNewPriority()) {
                    case 1:
                        workOrderCounts.setPriority1(1);
                        break;
                    case 2:
                        workOrderCounts.setPriority2(1);
                        break;
                    case 3:
                        workOrderCounts.setPriority3(1);
                        break;
                    case 4:
                        workOrderCounts.setPriority4(1);
                        break;
                    default:
                        break;
                }
                workOrderCounts.setTotalCount(1);
            }
            
            // Set the Priority equal to the new Priority
            workOrderVO.setPriority(workOrderVO.getNewPriority());
            
            // Set the new WorkOrderTrends field
            if (workOrderVO.getTrendDetails() != null && !workOrderVO.getTrendDetails().isEmpty()) {
                if ((workOrderTrendList = DelegateUtil.getWorkOrderTrends(workOrderVO)) != null) {
                    
                    // Setting the rowId fields equal to Originals if there are any orginals, else use new values
                    for (int i = 0; i < originalWorkOrderTrendList.size(); i++) {
                        if (workOrderTrendList.get(i).getRowId() != null && i < workOrderTrendList.size()) {
                            workOrderTrendList.get(i).setRowId(workOrderTrendList.get(i).getRowId());
                        } else {
                            break;
                        }
                    }
                    
                }
            }
            
            // Set the new OpenWorkOrders field
            openWorkOrders = DelegateUtil.getOpenWorkOrders(workOrderVO);
            
            // Delete Original and Insert updated entities into Cassandra if original entities are found
            try { 
                 if (((errorMsg = ServiceUtil.validateObjectData(openWorkOrders)) == null) &&
                        ((errorMsg = originalWorkOrderCounts != null ? ServiceUtil.validateObjectData(originalWorkOrderCounts) : ServiceUtil.validateObjectData(workOrderCounts)) == null) &&
                        ((errorMsg = ServiceUtil.validateObjects(workOrderTrendList, Boolean.TRUE)) == null)) {

                    // Delete Original Items
                    if (originalOpenWorkOrders != null) {
                        openWorkOrdersDAO.delete(originalOpenWorkOrders);
                    }
                    if (originalWorkOrderTrendList != null && !originalWorkOrderTrendList.isEmpty()) {
                        workOrderTrendsDAO.deleteByCustomerSiteAreaAssetPriorityDate(originalWorkOrderTrendList.get(0).getCustomerAccount(), originalWorkOrderTrendList.get(0).getSiteId(), originalWorkOrderTrendList.get(0).getAreaId(), originalWorkOrderTrendList.get(0).getAssetId(), originalWorkOrderTrendList.get(0).getPriority(), originalWorkOrderTrendList.get(0).getCreatedDate());
                    }

                    // Insert New Items
                    openWorkOrdersDAO.create(openWorkOrders);
                    if (workOrderTrendList != null) {
                        workOrderTrendList.forEach(wt -> workOrderTrendsDAO.create(wt));
                    }
                    if (originalWorkOrderCounts != null) {
                        workOrderCountsDAO.update(originalWorkOrderCounts);
                    } else if (workOrderCounts != null) {
                        workOrderCountsDAO.create(workOrderCounts);
                    }

                    return "{\"outcome\":\"Updated Open WorkOrder items successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to update Open WorkOrder items.\"}";
        }
        return "{\"outcome\":\"WorkOrderVO is null.\"}";
    }
}
