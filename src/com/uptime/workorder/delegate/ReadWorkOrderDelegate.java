/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.workorder.dao.ClosedWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrderCountsDAO;
import com.uptime.cassandra.workorder.dao.WorkOrderTrendsDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.ClosedWorkOrders;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadWorkOrderDelegate {
    private final OpenWorkOrdersDAO openWorkOrdersDAO;
    private final ClosedWorkOrdersDAO closedWorkOrdersDAO;
    private final WorkOrderTrendsDAO workOrderTrendsDAO;
    private final WorkOrderCountsDAO workOrderCountsDAO;
    
    /**
     * Constructor
     */
    public ReadWorkOrderDelegate() {
        openWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
        closedWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().closedWorkOrdersDAO();
        workOrderTrendsDAO = WorkOrdersMapperImpl.getInstance().workOrderTrendsDAO();
        workOrderCountsDAO = WorkOrdersMapperImpl.getInstance().workOrderCountsDAO();
    }

    /**
     * Returns a List of OpenWorkOrders Objects for the given customer account and site id from open_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of OpenWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<OpenWorkOrders> getOpenWorkOrdersByCustomerSite(String customerAccount, UUID siteId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return openWorkOrdersDAO.findByCustomerSiteId(customerAccount, siteId);
    }

    /**
     * Returns a List of OpenWorkOrders Objects for the given customer account, site id, and area id from open_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of OpenWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<OpenWorkOrders> getOpenWorkOrdersByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return openWorkOrdersDAO.findByCustomerSiteArea(customerAccount, siteId, areaId);
    }

    /**
     * Returns a List of OpenWorkOrders Objects for the given customer account, site id, and area id from open_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of OpenWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<OpenWorkOrders> getOpenWorkOrdersByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return openWorkOrdersDAO.findByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
    }

    /**
     * Returns a List of OpenWorkOrders Objects for the given customer account, site id, and area id from open_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @return List of OpenWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<OpenWorkOrders> getOpenWorkOrdersByCustomerSiteAreaAssetPriority(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return openWorkOrdersDAO.findByCustomerSiteAreaAssetPriority(customerAccount, siteId, areaId, assetId, priority);
    }

    /**
     * Returns a List of OpenWorkOrders Objects for the given customer account, site id, and area id from open_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @return List of OpenWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<OpenWorkOrders> getOpenWorkOrdersByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return openWorkOrdersDAO.findByPK(customerAccount, siteId, areaId, assetId, priority, createdDate);
    }
    
    /**
     * Returns a List of WorkOrderTrends Objects for the given customer account, site id, and area id from work_order_trends table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @return List of WorkOrderTrends Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<WorkOrderTrends> getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return workOrderTrendsDAO.findByCustomerSiteAreaAssetPriorityDate(customerAccount, siteId, areaId, assetId, priority, createdDate);
    }

    /**
     * Returns a List of WorkOrderTrends Objects for the given customer account, site id, and area id from work_order_trends table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @param rowId, UUID Object
     * @return List of WorkOrderTrends Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<WorkOrderTrends> getWorkOrderTrendsByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate, UUID rowId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return workOrderTrendsDAO.findByPK(customerAccount, siteId, areaId, assetId, priority, createdDate, rowId);
    }
    
    /**
     * Returns a List of WorkOrderCounts Objects for the given customer account and site id from work_order_counts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<WorkOrderCounts> getWorkOrderCountsByCustomerSite(String customerAccount, UUID siteId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return workOrderCountsDAO.findByCustomerSiteId(customerAccount, siteId);
    }
    
    /**
     * Returns a List of WorkOrderCounts Objects for the given customer account, site id, and area id from work_order_counts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<WorkOrderCounts> getWorkOrderCountsByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return workOrderCountsDAO.findByCustomerSiteArea(customerAccount, siteId, areaId);
    }
    
    /**
     * Returns a List of WorkOrderCounts Objects for the given customer account, site id, and area id from work_order_counts table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of WorkOrderCounts Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<WorkOrderCounts> getWorkOrderCountsByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return workOrderCountsDAO.findByPK(customerAccount, siteId, areaId, assetId);
    }
    
    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id and year from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<ClosedWorkOrders> getClosedWorkOrdersByCustomerSiteYear(String customerAccount, UUID siteId, short year) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return closedWorkOrdersDAO.findByCustomerSiteYear(customerAccount, siteId, year);
    }
    
    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year and area id from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<ClosedWorkOrders> getClosedWorkOrdersByCustomerSiteYearArea(String customerAccount, UUID siteId, short year, UUID areaId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return closedWorkOrdersDAO.findByCustomerSiteYearArea(customerAccount, siteId, year, areaId);
    }
    
    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year, area id and asset id from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<ClosedWorkOrders> getClosedWorkOrdersByCustomerSiteYearAreaAsset(String customerAccount, UUID siteId, short year, UUID areaId, UUID assetId) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return closedWorkOrdersDAO.findByCustomerSiteYearAreaAsset(customerAccount, siteId, year, areaId, assetId);
    }
    
    /**
     * Returns a List of ClosedWorkOrders Objects for the given customer account, site id, year, area id, asset id and createdDate from closed_work_orders table
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param createdDate, Instant Object
     * @return List of ClosedWorkOrders Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if a given value is null
     */
    public List<ClosedWorkOrders> getClosedWorkOrdersByPK(String customerAccount, UUID siteId, short year, UUID areaId, UUID assetId, Instant createdDate) throws IllegalArgumentException, UnavailableException, ReadTimeoutException {
        return closedWorkOrdersDAO.findByPK(customerAccount, siteId, year, areaId, assetId, createdDate);
    }
}
