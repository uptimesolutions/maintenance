/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.uptime.services.util.HttpUtils.parseInstant;
import com.uptime.workorder.vo.WorkOrderVO;
import com.uptime.workorder.vo.TrendDetailsVO;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class JsonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class.getName());

    /**
     * Parse the given json String Object and return a WorkOrderVO object
     *
     * @param content, String Object
     * @return WorkOrderVO Object
     */
    public static WorkOrderVO parser(String content) {
        WorkOrderVO workOrderVO = null;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;
        LocalDateTime localDateTime;
        List<TrendDetailsVO> trendList;
        TrendDetailsVO tvo;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            workOrderVO = new WorkOrderVO();
            if (jsonObject.has("customerAccount")) {
                try {
                    workOrderVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing customerAccount : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("siteId")) {
                try {
                    workOrderVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing siteId: {}", ex.getMessage());
                }
            }
            if (jsonObject.has("areaId")) {
                try {
                    workOrderVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing areaId: {}", ex.getMessage());
                }
            }
            if (jsonObject.has("areaName")) {
                try {
                    workOrderVO.setAreaName(jsonObject.get("areaName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing areaName: {}", ex.getMessage());
                }
            }
            if (jsonObject.has("assetId")) {
                try {
                    workOrderVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing assetId: {}", ex.getMessage());
                }
            }
            if (jsonObject.has("assetName")) {
                try {
                    workOrderVO.setAssetName(jsonObject.get("assetName").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing assetName: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("clientAssetId")) {
                try {
                    workOrderVO.setClientAssetId(jsonObject.get("clientAssetId").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing clientAssetId : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("priority")) {
                try {
                    workOrderVO.setPriority(jsonObject.get("priority").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing priority : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("newPriority")) {
                try {
                    workOrderVO.setNewPriority(jsonObject.get("newPriority").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing newPriority : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("createdDate")) {
                try {
                    workOrderVO.setCreatedDate(parseInstant(jsonObject.get("createdDate").getAsString()));
                    localDateTime = LocalDateTime.ofInstant(workOrderVO.getCreatedDate(), ZoneOffset.UTC);
                    workOrderVO.setOpenedYear((short) localDateTime.getYear());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing createdDate : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("closeDate")) {
                try {
                    workOrderVO.setCloseDate(parseInstant(jsonObject.get("closeDate").getAsString()));
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing closeDate : {}", ex.getMessage());
                }
            }

            if (jsonObject.has("openedYear")) {
                try {
                    workOrderVO.setOpenedYear(jsonObject.get("openedYear").getAsShort());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing openedYear : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("action")) {
                try {
                    workOrderVO.setAction(jsonObject.get("action").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing action : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("assetComponent")) {
                try {
                    workOrderVO.setAssetComponent(jsonObject.get("assetComponent").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing assetComponent : {}", ex.getMessage());
                }
            }

            if (jsonObject.has("description")) {
                try {
                    workOrderVO.setDescription(jsonObject.get("description").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing description : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("notes")) {
                try {
                    workOrderVO.setNotes(jsonObject.get("notes").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing notes : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("issue")) {
                try {
                    workOrderVO.setIssue(jsonObject.get("issue").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing issue: {}", ex.getMessage());
                }
            }

            if (jsonObject.has("user")) {
                try {
                    workOrderVO.setUser(jsonObject.get("user").getAsString());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing user : {}", ex.getMessage());
                }
            }

            if (jsonObject.has("priority1")) {
                try {
                    workOrderVO.setPriority1(jsonObject.get("priority1").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing priority1 : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("priority2")) {
                try {
                    workOrderVO.setPriority2(jsonObject.get("priority2").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing priority2 : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("priority3")) {
                try {
                    workOrderVO.setPriority3(jsonObject.get("priority3").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing priority3 : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("priority4")) {
                try {
                    workOrderVO.setPriority4(jsonObject.get("priority4").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing priority4 : {}", ex.getMessage());
                }
            }
            if (jsonObject.has("totalCount")) {
                try {
                    workOrderVO.setTotalCount(jsonObject.get("totalCount").getAsByte());
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing totalCount : {}", ex.getMessage());
                }
            }

            if (jsonObject.has("trendDetails")) {
                try {
                    if ((jsonArray = jsonObject.getAsJsonArray("trendDetails")) != null && !jsonArray.isEmpty()) {
                        trendList = new ArrayList();

                        for (int j = 0; j < jsonArray.size(); j++) {
                            jsonObject = jsonArray.get(j).getAsJsonObject();
                            tvo = new TrendDetailsVO();

                            if (jsonObject.has("pointLocationId")) {
                                try {
                                    tvo.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString()));
                                } catch (Exception ex) {
                                    LOGGER.error("Exception parsing pointLocationId: {}", ex.getMessage());
                                }
                            }
                            if (jsonObject.has("pointId")) {
                                try {
                                    tvo.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString()));
                                } catch (Exception ex) {
                                    LOGGER.error("Exception parsing pointId : {}", ex.getMessage());
                                }
                            }
                            if (jsonObject.has("rowId")) {
                                try {
                                    tvo.setRowId(UUID.fromString(jsonObject.get("rowId").getAsString()));
                                } catch (Exception ex) {
                                    LOGGER.error("Exception parsing rowId : {}", ex.getMessage());
                                }
                            }
                            if (jsonObject.has("apSetId")) {
                                try {
                                    tvo.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                                } catch (Exception ex) {
                                    LOGGER.error("Exception parsing apSetId : {}", ex.getMessage());
                                }
                            }

                            if (jsonObject.has("paramName")) {
                                try {
                                    tvo.setParamName(jsonObject.get("paramName").getAsString());
                                } catch (Exception ex) {
                                    LOGGER.error("Exception parsing paramName : {}", ex.getMessage());
                                }
                            }
                            if (jsonObject.has("channelType")) {
                                try {
                                    tvo.setChannelType(jsonObject.get("channelType").getAsString());
                                } catch (Exception ex) {
                                    LOGGER.error("Exception parsing channelType : {}", ex.getMessage());
                                }
                            }
                            trendList.add(tvo);
                        }
                        workOrderVO.setTrendDetails(trendList);
                    }
                } catch (Exception ex) {
                    LOGGER.error("Exception parsing TrendDetails : {}", ex.getMessage());
                }
            }
        }

        return workOrderVO;
    }
}
