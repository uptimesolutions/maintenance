/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.utils;

import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import com.uptime.cassandra.workorder.entity.ClosedWorkOrders;
import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import com.uptime.cassandra.workorder.entity.WorkOrderRecord;
import com.uptime.workorder.vo.TrendDetailsVO;
import com.uptime.workorder.vo.WorkOrderVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class DelegateUtil {
    
    /**
     * Convert WorkOrderVO Object to OpenWorkOrders Object
     * @param workOrderVO, WorkOrderVO Object
     * @return OpenWorkOrders Object
     */
    public static OpenWorkOrders getOpenWorkOrders(WorkOrderVO workOrderVO) {
        OpenWorkOrders entity = new OpenWorkOrders();
        
        if(workOrderVO != null) {
            entity.setAction(workOrderVO.getAction());
            entity.setAreaId(workOrderVO.getAreaId());
            entity.setAreaName(workOrderVO.getAreaName());
            entity.setAssetComponent(workOrderVO.getAssetComponent());
            entity.setAssetId(workOrderVO.getAssetId());
            entity.setAssetName(workOrderVO.getAssetName());
            entity.setCreatedDate(workOrderVO.getCreatedDate());
            entity.setCustomerAccount(workOrderVO.getCustomerAccount());
            entity.setDescription(workOrderVO.getDescription());
            entity.setIssue(workOrderVO.getIssue());
            entity.setNotes(workOrderVO.getNotes());
            entity.setPriority(workOrderVO.getPriority());
            entity.setSiteId(workOrderVO.getSiteId());
            entity.setUser(workOrderVO.getUser());
        }
        
        return entity;
    }
    
    /**
     * Convert WorkOrderVO Object to List of WorkOrderTrends Objects
     * @param workOrderVO, WorkOrderVO Object
     * @return WorkOrderTrends Object
     */
    public static List<WorkOrderTrends> getWorkOrderTrends(WorkOrderVO workOrderVO) {
        List<WorkOrderTrends> list = new ArrayList();
        WorkOrderTrends entity;
        
        if (workOrderVO != null && workOrderVO.getTrendDetails() != null && !workOrderVO.getTrendDetails().isEmpty()) {
            for (TrendDetailsVO tvo : workOrderVO.getTrendDetails()) {
                entity = new WorkOrderTrends();
                entity.setCustomerAccount(workOrderVO.getCustomerAccount());
                entity.setSiteId(workOrderVO.getSiteId());
                entity.setAreaId(workOrderVO.getAreaId());
                entity.setAssetId(workOrderVO.getAssetId());
                entity.setPriority(workOrderVO.getPriority());
                entity.setCreatedDate(workOrderVO.getCreatedDate());
                entity.setRowId(UUID.randomUUID());
                entity.setApSetId(tvo.getApSetId());
                entity.setChannelType(tvo.getChannelType());
                entity.setParamName(tvo.getParamName());
                entity.setPointId(tvo.getPointId());
                entity.setPointLocationId(tvo.getPointLocationId());
                list.add(entity);
            }
        }
        return list;
    }
    
    /**
     * Convert WorkOrderVO Object to WorkOrderCounts Object
     * @param workOrderVO, WorkOrderVO Object
     * @return WorkOrderCounts Object
     */
    public static WorkOrderCounts getWorkOrderCounts(WorkOrderVO workOrderVO) {
        WorkOrderCounts entity = new WorkOrderCounts();
        
        if(workOrderVO != null) {
            entity.setCustomerAccount(workOrderVO.getCustomerAccount());
            entity.setSiteId(workOrderVO.getSiteId());
            entity.setAreaId(workOrderVO.getAreaId());
            entity.setAssetId(workOrderVO.getAssetId());
            entity.setPriority1(workOrderVO.getPriority1());
            entity.setPriority2(workOrderVO.getPriority2());
            entity.setPriority3(workOrderVO.getPriority3());
            entity.setPriority4(workOrderVO.getPriority4());
            entity.setTotalCount(workOrderVO.getTotalCount());
        }
        
        return entity;
    }
    
    /**
     * Convert WorkOrderVO Object to ClosedWorkOrders Object
     * @param workOrderVO, WorkOrderVO Object
     * @return ClosedWorkOrders Object
     */
    public static ClosedWorkOrders getClosedWorkOrders(WorkOrderVO workOrderVO) {
        ClosedWorkOrders entity = new ClosedWorkOrders();
        WorkOrderRecord workOrderRecord;
        
        if(workOrderVO != null) {
            entity.setCustomerAccount(workOrderVO.getCustomerAccount());
            entity.setSiteId(workOrderVO.getSiteId());
            entity.setOpenedYear(workOrderVO.getOpenedYear());
            entity.setAreaId(workOrderVO.getAreaId());
            entity.setAssetId(workOrderVO.getAssetId());
            entity.setCreatedDate(workOrderVO.getCreatedDate());
            
            workOrderRecord = new WorkOrderRecord();
            workOrderRecord.setAction(workOrderVO.getAction());
            workOrderRecord.setAreaName(workOrderVO.getAreaName());
            workOrderRecord.setAssetComponent(workOrderVO.getAssetComponent());
            workOrderRecord.setAssetName(workOrderVO.getAssetName());
            workOrderRecord.setCloseDate(workOrderVO.getCloseDate());
            workOrderRecord.setDescription(workOrderVO.getDescription());
            workOrderRecord.setIssue(workOrderVO.getIssue());
            workOrderRecord.setNotes(workOrderVO.getNotes());
            workOrderRecord.setPriority("" + workOrderVO.getPriority());
            workOrderRecord.setUser(workOrderVO.getUser());
            
            entity.setWorkOrderDetails(workOrderRecord);
        }
        
        return entity;
    }

}