/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.vo;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class WorkOrderVO {
   private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private String clientAssetId;
    private byte priority;
    private byte newPriority;
    private Instant createdDate;
    private Instant closeDate;
    private short openedYear;
    private String action;
    private String areaName;
    private String assetComponent;
    private String assetName;
    private String description;
    private String issue;
    private String notes;
    private String user;
    private int priority1;
    private int priority2;
    private int priority3;
    private int priority4;
    private int totalCount;
    private List<TrendDetailsVO> trendDetails;
    
    public WorkOrderVO() {
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getClientAssetId() {
        return clientAssetId;
    }

    public void setClientAssetId(String clientAssetId) {
        this.clientAssetId = clientAssetId;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public byte getNewPriority() {
        return newPriority;
    }

    public void setNewPriority(byte newPriority) {
        this.newPriority = newPriority;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Instant closeDate) {
        this.closeDate = closeDate;
    }

    public short getOpenedYear() {
        return openedYear;
    }

    public void setOpenedYear(short openedYear) {
        this.openedYear = openedYear;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetComponent() {
        return assetComponent;
    }

    public void setAssetComponent(String assetComponent) {
        this.assetComponent = assetComponent;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPriority1() {
        return priority1;
    }

    public void setPriority1(int priority1) {
        this.priority1 = priority1;
    }

    public int getPriority2() {
        return priority2;
    }

    public void setPriority2(int priority2) {
        this.priority2 = priority2;
    }

    public int getPriority3() {
        return priority3;
    }

    public void setPriority3(int priority3) {
        this.priority3 = priority3;
    }

    public int getPriority4() {
        return priority4;
    }

    public void setPriority4(int priority4) {
        this.priority4 = priority4;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<TrendDetailsVO> getTrendDetails() {
        return trendDetails;
    }

    public void setTrendDetails(List<TrendDetailsVO> trendDetails) {
        this.trendDetails = trendDetails;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.areaId);
        hash = 97 * hash + Objects.hashCode(this.assetId);
        hash = 97 * hash + Objects.hashCode(this.clientAssetId);
        hash = 97 * hash + this.priority;
        hash = 97 * hash + this.newPriority;
        hash = 97 * hash + Objects.hashCode(this.createdDate);
        hash = 97 * hash + Objects.hashCode(this.closeDate);
        hash = 97 * hash + this.openedYear;
        hash = 97 * hash + Objects.hashCode(this.action);
        hash = 97 * hash + Objects.hashCode(this.areaName);
        hash = 97 * hash + Objects.hashCode(this.assetComponent);
        hash = 97 * hash + Objects.hashCode(this.assetName);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + Objects.hashCode(this.issue);
        hash = 97 * hash + Objects.hashCode(this.notes);
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + this.priority1;
        hash = 97 * hash + this.priority2;
        hash = 97 * hash + this.priority3;
        hash = 97 * hash + this.priority4;
        hash = 97 * hash + this.totalCount;
        hash = 97 * hash + Objects.hashCode(this.trendDetails);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrderVO other = (WorkOrderVO) obj;
        if (this.priority != other.priority) {
            return false;
        }
        if (this.newPriority != other.newPriority) {
            return false;
        }
        if (this.openedYear != other.openedYear) {
            return false;
        }
        if (this.priority1 != other.priority1) {
            return false;
        }
        if (this.priority2 != other.priority2) {
            return false;
        }
        if (this.priority3 != other.priority3) {
            return false;
        }
        if (this.priority4 != other.priority4) {
            return false;
        }
        if (this.totalCount != other.totalCount) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.clientAssetId, other.clientAssetId)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetComponent, other.assetComponent)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.issue, other.issue)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        if (!Objects.equals(this.closeDate, other.closeDate)) {
            return false;
        }
        if (!Objects.equals(this.trendDetails, other.trendDetails)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WorkOrderVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", clientAssetId=" + clientAssetId + ", priority=" + priority + ", newPriority=" + newPriority + ", createdDate=" + createdDate + ", closeDate=" + closeDate + ", openedYear=" + openedYear + ", action=" + action + ", areaName=" + areaName + ", assetComponent=" + assetComponent + ", assetName=" + assetName + ", description=" + description + ", issue=" + issue + ", notes=" + notes + ", user=" + user + ", priority1=" + priority1 + ", priority2=" + priority2 + ", priority3=" + priority3 + ", priority4=" + priority4 + ", totalCount=" + totalCount + ", trendDetails=" + trendDetails + '}';
    }
}
