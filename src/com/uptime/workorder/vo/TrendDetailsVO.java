/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class TrendDetailsVO {
    private UUID pointLocationId;
    private UUID pointId;
    private UUID rowId;
    private UUID apSetId;
    private String paramName;
    private String channelType;

    public TrendDetailsVO() {
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getRowId() {
        return rowId;
    }

    public void setRowId(UUID rowId) {
        this.rowId = rowId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.pointLocationId);
        hash = 61 * hash + Objects.hashCode(this.pointId);
        hash = 61 * hash + Objects.hashCode(this.rowId);
        hash = 61 * hash + Objects.hashCode(this.apSetId);
        hash = 61 * hash + Objects.hashCode(this.paramName);
        hash = 61 * hash + Objects.hashCode(this.channelType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrendDetailsVO other = (TrendDetailsVO) obj;
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.rowId, other.rowId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TrendDetailsVO{" + "pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", rowId=" + rowId + ", apSetId=" + apSetId + ", paramName=" + paramName + ", channelType=" + channelType + '}';
    }
    
}
