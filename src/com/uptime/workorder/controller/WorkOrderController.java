/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.workorder.entity.ClosedWorkOrders;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import com.uptime.cassandra.workorder.entity.WorkOrderCounts;
import com.uptime.cassandra.workorder.entity.WorkOrderTrends;
import static com.uptime.services.util.HttpUtils.parseInstant;
import com.uptime.workorder.delegate.CreateWorkOrderDelegate;
import com.uptime.workorder.delegate.ReadWorkOrderDelegate;
import com.uptime.workorder.utils.JsonUtil;
import com.uptime.workorder.vo.WorkOrderVO;
import com.uptime.services.util.JsonConverterUtil;
import static com.uptime.workorder.WorkOrderService.sendEvent;
import com.uptime.workorder.delegate.UpdateWorkOrderDelegate;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class WorkOrderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkOrderController.class.getName());
    ReadWorkOrderDelegate readDelegate;

    /**
     * Controller
     */
    public WorkOrderController() {
        readDelegate = new ReadWorkOrderDelegate();
    }

    /**
     * Return a json of a list of OpenWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getOpenWorkOrdersByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<OpenWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getOpenWorkOrdersByCustomerSite(customerAccount, UUID.fromString(siteId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"openWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of OpenWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getOpenWorkOrdersByCustomerSiteArea(String customerAccount, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<OpenWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getOpenWorkOrdersByCustomerSiteArea(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"openWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of OpenWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getOpenWorkOrdersByCustomerSiteAreaAsset(String customerAccount, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<OpenWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getOpenWorkOrdersByCustomerSiteAreaAsset(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"openWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of OpenWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param priority, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getOpenWorkOrdersByCustomerSiteAreaAssetPriority(String customerAccount, String siteId, String areaId, String assetId, String priority) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<OpenWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getOpenWorkOrdersByCustomerSiteAreaAssetPriority(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), (byte) Integer.parseInt(priority))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"priority\":\"").append(priority).append("\",")
                    .append("\"openWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of OpenWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param priority, String Object
     * @param createdDate, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getOpenWorkOrdersByPK(String customerAccount, String siteId, String areaId, String assetId, String priority, String createdDate) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<OpenWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getOpenWorkOrdersByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), (byte) Integer.parseInt(priority), parseInstant(createdDate))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"priority\":\"").append(priority).append("\",")
                    .append("\"createdDate\":\"").append(createdDate).append("\",")
                    .append("\"openWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of WorkOrderTrends Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param priority, String Object
     * @param createdDate, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(String customerAccount, String siteId, String areaId, String assetId, String priority, String createdDate) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<WorkOrderTrends> result;
        StringBuilder json;

        if ((result = readDelegate.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), (byte) Integer.parseInt(priority), parseInstant(createdDate))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"priority\":\"").append(priority).append("\",")
                    .append("\"createdDate\":\"").append(createdDate).append("\",")
                    .append("\"workOrderTrends\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of OpenWorkOrders Objects, WorkOrderTrends Objects or both by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param priority, String Object
     * @param createdDate, String Object
     * @param returnOpenWorkOrders, boolean
     * @param returnWorkOrderTrends, boolean
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getItemsByCustomerSiteAreaAssetPriorityDate(String customerAccount, String siteId, String areaId, String assetId, String priority, String createdDate, boolean returnOpenWorkOrders, boolean returnWorkOrderTrends) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        boolean found;
        StringBuilder json;
        List<OpenWorkOrders> resultOpenWorkOrders;
        List<WorkOrderTrends> resultWorkOrderTrends;

        json = new StringBuilder();
        json
                .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"assetId\":\"").append(assetId).append("\",")
                .append("\"priority\":\"").append(priority).append("\",")
                .append("\"createdDate\":\"").append(createdDate).append("\",");

        found = false;
        if (returnOpenWorkOrders) {
            if ((resultOpenWorkOrders = readDelegate.getOpenWorkOrdersByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), (byte) Integer.parseInt(priority), parseInstant(createdDate))) != null && !resultOpenWorkOrders.isEmpty()) {
                found = true;

                json.append("\"openWorkOrders\":[");
                for (int i = 0; i < resultOpenWorkOrders.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(resultOpenWorkOrders.get(i))).append(i < resultOpenWorkOrders.size() - 1 ? "," : "");
                }
                json.append("],");
            }
        }

        if (returnWorkOrderTrends) {
            if ((resultWorkOrderTrends = readDelegate.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), (byte) Integer.parseInt(priority), parseInstant(createdDate))) != null && !resultWorkOrderTrends.isEmpty()) {
                found = true;
                json.append("\"workOrderTrends\":[");
                for (int i = 0; i < resultWorkOrderTrends.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(resultWorkOrderTrends.get(i))).append(i < resultWorkOrderTrends.size() - 1 ? "," : "");
                }
                json.append("],");
            }
        }

        if (found) {
            json.append("\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of WorkOrderTrends Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param priority, String Object
     * @param createdDate, String Object
     * @param rowId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getWorkOrderTrendsByPK(String customerAccount, String siteId, String areaId, String assetId, String priority, String createdDate, String rowId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<WorkOrderTrends> result;
        StringBuilder json;

        if ((result = readDelegate.getWorkOrderTrendsByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), (byte) Integer.parseInt(priority), parseInstant(createdDate), UUID.fromString(rowId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"priority\":\"").append(priority).append("\",")
                    .append("\"createdDate\":\"").append(createdDate).append("\",")
                    .append("\"rowId\":\"").append(rowId).append("\",")
                    .append("\"workOrderTrends\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of WorkOrderCounts Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getWorkOrderCountsByCustomerSite(String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<WorkOrderCounts> result;
        StringBuilder json;

        if ((result = readDelegate.getWorkOrderCountsByCustomerSite(customerAccount, UUID.fromString(siteId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"workOrderCounts\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of WorkOrderCounts Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getWorkOrderCountsByCustomerSiteArea(String customerAccount, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<WorkOrderCounts> result;
        StringBuilder json;

        if ((result = readDelegate.getWorkOrderCountsByCustomerSiteArea(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"workOrderCounts\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of WorkOrderCounts Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getWorkOrderCountsByPK(String customerAccount, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<WorkOrderCounts> result;
        StringBuilder json;

        if ((result = readDelegate.getWorkOrderCountsByPK(customerAccount, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"workOrderCounts\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of ClosedWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param year, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getClosedWorkOrdersByCustomerSiteYear(String customerAccount, String siteId, String year) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ClosedWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getClosedWorkOrdersByCustomerSiteYear(customerAccount, UUID.fromString(siteId), Short.parseShort(year))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"year\":\"").append(year).append("\",")
                    .append("\"closedWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of ClosedWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param year, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getByCustomerSiteYearArea(String customerAccount, String siteId, String year, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ClosedWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getClosedWorkOrdersByCustomerSiteYearArea(customerAccount, UUID.fromString(siteId), Short.parseShort(year), UUID.fromString(areaId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"year\":\"").append(year).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"closedWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of ClosedWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param year, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getClosedWorkOrdersByCustomerSiteYearAreaAsset(String customerAccount, String siteId, String year, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ClosedWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getClosedWorkOrdersByCustomerSiteYearAreaAsset(customerAccount, UUID.fromString(siteId), Short.parseShort(year), UUID.fromString(areaId), UUID.fromString(assetId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"year\":\"").append(year).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"closedWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of ClosedWorkOrders Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param year, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param createdDate, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getClosedWorkOrdersByPK(String customerAccount, String siteId, String year, String areaId, String assetId, String createdDate) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ClosedWorkOrders> result;
        StringBuilder json;

        if ((result = readDelegate.getClosedWorkOrdersByPK(customerAccount, UUID.fromString(siteId), Short.parseShort(year), UUID.fromString(areaId), UUID.fromString(assetId), parseInstant(createdDate))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"year\":\"").append(year).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"createdDate\":\"").append(createdDate).append("\",")
                    .append("\"closedWorkOrders\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new OpenWorkOrder by inserting into multiple tables
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createOpenWorkOrder(String content) throws IllegalArgumentException {
        CreateWorkOrderDelegate createDelegate;
        WorkOrderVO workOrderVO;

        if ((workOrderVO = JsonUtil.parser(content)) != null) {
            if (workOrderVO.getSiteId() != null
                    && workOrderVO.getCustomerAccount() != null && !workOrderVO.getCustomerAccount().isEmpty()
                    && workOrderVO.getSiteId() != null
                    && workOrderVO.getAreaId() != null && workOrderVO.getAssetId() != null
                    && workOrderVO.getAreaName() != null && !workOrderVO.getAreaName().isEmpty()
                    && workOrderVO.getAssetName() != null && !workOrderVO.getAssetName().isEmpty()
                    && workOrderVO.getPriority() > 0 && workOrderVO.getPriority() < 5) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateWorkOrderDelegate();
                    return createDelegate.createOpenWorkOrder(workOrderVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    sendEvent(e.getStackTrace());
                    return "{\"outcome\":\"Error: Failed to create new WorkOrder.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Create a new ClosedWorkOrder by inserting/Deleting into multiple tables
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createClosedWorkOrder(String content) throws IllegalArgumentException {
        CreateWorkOrderDelegate createDelegate;
        WorkOrderVO workOrderVO;

        if ((workOrderVO = JsonUtil.parser(content)) != null) {
            if (workOrderVO != null && workOrderVO.getSiteId() != null
                    && workOrderVO.getCustomerAccount() != null && !workOrderVO.getCustomerAccount().isEmpty()
                    && workOrderVO.getSiteId() != null
                    && workOrderVO.getAreaId() != null && workOrderVO.getAssetId() != null
                    && workOrderVO.getAreaName() != null && !workOrderVO.getAreaName().isEmpty()
                    && workOrderVO.getAssetName() != null && !workOrderVO.getAssetName().isEmpty()
                    && workOrderVO.getPriority() > 0 && workOrderVO.getPriority() < 5
                    && workOrderVO.getCloseDate() != null) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateWorkOrderDelegate();
                    return createDelegate.createClosedWorkOrder(workOrderVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    sendEvent(e.getStackTrace());
                    return "{\"outcome\":\"Error: Failed to close WorkOrder.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given OpenWorkOrder
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateOpenWorkOrder(String content) {
        UpdateWorkOrderDelegate updateDelegate;
        WorkOrderVO workOrderVO;

        if ((workOrderVO = JsonUtil.parser(content)) != null) {

            if (workOrderVO != null && workOrderVO.getSiteId() != null
                    && workOrderVO.getCustomerAccount() != null && !workOrderVO.getCustomerAccount().isEmpty()
                    && workOrderVO.getAreaId() != null && workOrderVO.getAssetId() != null
                    && workOrderVO.getAreaName() != null && !workOrderVO.getAreaName().isEmpty()
                    && workOrderVO.getAssetName() != null && !workOrderVO.getAssetName().isEmpty()
                    && workOrderVO.getAreaName() != null && !workOrderVO.getAreaName().isEmpty()
                    && workOrderVO.getPriority() > 0 && workOrderVO.getPriority() < 5
                    && workOrderVO.getNewPriority() > 0 && workOrderVO.getNewPriority() < 5
                    && workOrderVO.getCreatedDate() != null) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateWorkOrderDelegate();
                    if (workOrderVO.getNewPriority() == workOrderVO.getPriority()) {
                        return updateDelegate.updateOpenWorkOrder(workOrderVO);
                    } else {
                        return updateDelegate.deleteOldAndCreateNewWorkOrder(workOrderVO);
                    }
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update Open WorkOrder items.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
