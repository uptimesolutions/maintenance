/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.workorder.http.handlers;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.uptime.services.http.handler.AbstractGenericHandlerNew;
import static com.uptime.workorder.WorkOrderService.running;
import static com.uptime.workorder.WorkOrderService.sendEvent;
import com.uptime.workorder.controller.WorkOrderController;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class WorkOrderHandler extends AbstractGenericHandlerNew {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkOrderHandler.class.getName());
    WorkOrderController controller;

    /**
     * Constructor
     */
    public WorkOrderHandler() {
        controller = new WorkOrderController();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        boolean returnWorkOrderTrends, returnOpenWorkOrders;
        StackTraceElement[] stackTrace = null;
        String resp;
        Map<String, Object> params;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        Object object;

        try {
            if ((params = parseQuery(he.getRequestURI().getRawQuery())).isEmpty()) {
                resp = "{\"outcome\":\"Hello World!\"}";
            } else {
                LOGGER.info("Query: {}", new Object[]{he.getRequestURI().getQuery()});
                for (String key : params.keySet()) {
                    if((object = params.get(key)) != null) {
                        LOGGER.info("{}: {}", new Object[]{key, object.toString()});
                    } else {
                        LOGGER.info("{}: null", new Object[]{key});
                    }
                }
                if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("priority") && params.containsKey("createdDate") && params.containsKey("rowId")) {
                    try {
                        resp = controller.getWorkOrderTrendsByPK(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("priority").toString(), params.get("createdDate").toString(), params.get("rowId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, areaId, assetId, priority, createdDate and rowId are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("priority") && params.containsKey("createdDate") && (params.containsKey("workOrderTrends") || params.containsKey("openWorkOrders"))) {
                    try {
                        returnWorkOrderTrends = false;
                        returnOpenWorkOrders = false;
                        
                        if (params.containsKey("workOrderTrends")) {
                            if (params.get("workOrderTrends").toString().equalsIgnoreCase("true"))
                                returnWorkOrderTrends = true;
                        }
                        if (params.containsKey("openWorkOrders")) {
                            if (params.get("openWorkOrders").toString().equalsIgnoreCase("true"))
                                returnOpenWorkOrders = true;
                        }
                        resp = controller.getItemsByCustomerSiteAreaAssetPriorityDate(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("priority").toString(), params.get("createdDate").toString(), returnOpenWorkOrders, returnWorkOrderTrends);
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, areaId, assetId, priority, createdDate, workOrderTrends and openWorkOrders are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("counts")) { 
                    try {
                        if (params.get("counts").toString().equalsIgnoreCase("true"))
                            resp = controller.getWorkOrderCountsByPK(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString());
                        else
                            resp = "{\"outcome\":\"A valid customer, siteId, areaId, assetId and counts are required.\"}";
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, areaId, assetId and counts are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("counts")) { 
                    try {
                        if (params.get("counts").toString().equalsIgnoreCase("true"))
                            resp = controller.getWorkOrderCountsByCustomerSiteArea(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString());
                        else
                            resp = "{\"outcome\":\"A valid customer, siteId, areaId and counts are required.\"}";
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, areaId and counts are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("counts")) { 
                    try {
                        if (params.get("counts").toString().equalsIgnoreCase("true"))
                            resp = controller.getWorkOrderCountsByCustomerSite(params.get("customer").toString(), params.get("siteId").toString());
                        else
                            resp = "{\"outcome\":\"A valid customer, siteId and counts are required.\"}";
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId and counts are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("openedYear") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("createdDate")) {
                    try {
                        resp = controller.getClosedWorkOrdersByPK(params.get("customer").toString(), params.get("siteId").toString(), params.get("openedYear").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("createdDate").toString());
                    } catch (NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, openedYear, areaId, assetId and createdDate are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("openedYear") && params.containsKey("areaId") && params.containsKey("assetId")) {
                    try {
                        resp = controller.getClosedWorkOrdersByCustomerSiteYearAreaAsset(params.get("customer").toString(), params.get("siteId").toString(), params.get("openedYear").toString(), params.get("areaId").toString(), params.get("assetId").toString());
                    } catch (NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, openedYear, areaId and assetId are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("openedYear") && params.containsKey("areaId")) {
                    try {
                        resp = controller.getByCustomerSiteYearArea(params.get("customer").toString(), params.get("siteId").toString(), params.get("openedYear").toString(), params.get("areaId").toString());
                    } catch (NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, openedYear and areaId are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("openedYear")) {
                    try {
                        resp = controller.getClosedWorkOrdersByCustomerSiteYear(params.get("customer").toString(), params.get("siteId").toString(), params.get("openedYear").toString());
                    } catch (NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId and openedYear are required.\"}";
                    }
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId") && params.containsKey("priority")) {
                    try {
                        resp = controller.getOpenWorkOrdersByCustomerSiteAreaAssetPriority(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString(), params.get("priority").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, areaId, assetId, and priority are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId") && params.containsKey("assetId")) { 
                    try {
                        resp = controller.getOpenWorkOrdersByCustomerSiteAreaAsset(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString(), params.get("assetId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, areaId, and assetId are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("areaId")) { 
                    try {
                        resp = controller.getOpenWorkOrdersByCustomerSiteArea(params.get("customer").toString(), params.get("siteId").toString(), params.get("areaId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId, and areaId are required.\"}";
                    } 
                } else if (params.containsKey("customer") && params.containsKey("siteId")) { 
                    try {
                        resp = controller.getOpenWorkOrdersByCustomerSite(params.get("customer").toString(), params.get("siteId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer and siteId are required.\"}";
                    } 
                } else {
                    resp = "{\"outcome\":\"Unknown Params\"}";
                }
            }
            
            response = resp.getBytes();
            if (resp.contains("Unknown Params") || (resp.contains("A valid") && resp.contains("required."))) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            } else if (resp.contains("Null value received from database.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (ReadTimeoutException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"ReadTimeoutException\"}".getBytes()), he);
        } catch (UnavailableException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnavailableException\"}".getBytes()), he);
        } catch (UnsupportedEncodingException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes()), he);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    if (content != null && content.contains("closeDate"))
                        resp = controller.createClosedWorkOrder(content);
                    else
                        resp = controller.createOpenWorkOrder(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
    /**
     * HTTP PUT handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.updateOpenWorkOrder(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
    /**
     * Returns the field 'running'
     * @return boolean
     */
    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Returns the field 'LOGGER'
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * Set the HttpExchange response header and return a StackTraceElement Array Object
     * @param exception, Exception Object
     * @param response, byte Array Object
     * @param httpExchange, HttpExchange Object
     * @return StackTraceElement Array Object
     * @throws IOException 
     */
    private StackTraceElement[] setStackTrace(final Exception exception, byte[] response, HttpExchange httpExchange) throws IOException {
        LOGGER.error(exception.getMessage(), exception);
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
        return exception.getStackTrace();
    }
}
